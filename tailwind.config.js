/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/**/*{html,js}'],
  theme: {
    fontFamily:{
      'mono': ['ui-monospace', 'SFMono-Regular', 'Menlo', 'Monaco', 'Consolas', 'monospace'],
    },
    backgroundImage: {
      'hero-contact': "url('images/contactbg.png')",
    },
    container: {
      center: true,
      padding: '16px',
    },
    extend: {
      colors:{
        border: '#14b8a6',
        ladding: '#e6de4c',
        stone: '#e7e5e4',
        footer: '#E2836A',
        hastag: '#d6d3d1',
        btn: '#fbbf24',
        cupid: '#be185d',
      },
    },
  },
  plugins: [],
}

